//
//  ViewController.swift
//  dz8
//
//  Created by Даниил Карпитский on 2/6/22.
//
//1
import UIKit

class ViewController: UIViewController {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        var line: Int = 0           // nomber of line
        var i: Int = 1              // position on the board
        var odd: Bool = true        // enum or odd line
        var xCor: Float = 0
        var yCor: Float = 250
        
        repeat{
            if odd == true {        //odd line
                repeat{
                    if i == 1{
                        xCor -= 52.5
                    }
                    xCor += 105
                    i += 1
                    let rectFrame: CGRect = CGRect(x:CGFloat(xCor),  y:CGFloat(yCor), width:CGFloat(52.5), height:CGFloat(52.5))
                    let blackView = UIView(frame: rectFrame)
                    blackView.backgroundColor = UIColor.black
                    self.view.addSubview(blackView)
                    
                    
                } while i != 5
            }
            else {                  //enum line
                repeat{
                    if i == 5{
                        xCor += 52.5
                    }
                    
                    i -= 1
                    xCor -= 105
                    let rectFrame: CGRect = CGRect(x:CGFloat(xCor), y:CGFloat(yCor), width:CGFloat(52.5), height:CGFloat(52.5))
                    let blackView = UIView(frame: rectFrame)
                    blackView.backgroundColor = UIColor.black
                    self.view.addSubview(blackView)
                    
                    
                } while i != 1
                
            }
            yCor += 52.5
            line += 1
            odd = !odd
            
        } while line != 8
        
        
        
    }
    
}

